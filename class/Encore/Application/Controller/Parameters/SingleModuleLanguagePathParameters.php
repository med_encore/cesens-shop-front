<?php

namespace Encore\Application\Controller\Parameters;


use Encore\Application\Application;
use Encore\Cesens\Web\Model\Language;

class SingleModuleLanguagePathParameters extends SingleModulePathParameters
{
    private $language;

    public function __construct(Application $application, $data = null)
    {
        parent::__construct($application, $data);
        foreach ($application->getConfig('i18n.languages', []) as $language) {
            if ($this->parts[0] === $language) {
                $this->language = Language::fromString($language);
                array_splice($this->parts, 0, 1);
                break;
            }
        }
    }

    public function getLanguage()
    {
        return $this->language;
    }

    public function setLanguage($language)
    {
        $this->language = !($language instanceof Language) ? Language::fromString($language) : $language;
    }

    public function getUri($action = null, $controller = null, $module = null, array $parameters = [])
    {
        $uri = parent::getUri($action, $controller, $module, $parameters);
        return !empty($this->language) ? '/' . $this->language . $uri : $uri;
    }

    public function getUrl($action = null, $controller = null, $module = null, array $parameters = [])
    {
        $url  = !empty($_SERVER['HTTPS']) ? 'https://' : 'http://';
        $url .= $_SERVER['HTTP_HOST'];
        $url .= $this->getUri($action, $controller, $module, $parameters);
        return $url;
    }
}
