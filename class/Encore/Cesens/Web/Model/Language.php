<?php

namespace Encore\Cesens\Web\Model;

use Encore\Application\Model\Model;

class Language extends Model
{
    private $language;
    private $country;
    private $preference;

    public static function fromString($string)
    {
        $parts  = explode(';', $string);
        $lang   = explode('-', str_replace('_', '-', $parts[0]));
        return self::create([
            'language'   => isset($lang[0])  ? trim($lang[0]) : null,
            'country'    => isset($lang[1])  ? trim($lang[1]) : null,
            'preference' => isset($parts[1]) ? trim(preg_replace('/q\s*=/', '', $parts[1])) : 1.0,
        ]);
    }

    public static function sort(Language $a, Language $b)
    {
        $a = $a->getPreference();
        $b = $b->getPreference();
        if ($a === $b) {
            return 0;
        }
        return $a < $b ? 1 : -1;
    }

    public static function rsort(Language $a, Language $b)
    {
        return -1 * self::sort($a, $b);
    }

    public function __toString()
    {
        return $this->getCode();
    }

    public function getCode()
    {
        $code = $this->getLanguage();
        $code = !empty($code) ? (!empty($this->getCountry()) ? $code . '-' . $this->getCountry() : $code) : null;
        return $code;
    }

    public function getLanguage()
    {
        return $this->language;
    }

    public function setLanguage($language)
    {
        $this->language = strtolower($language);
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = strtoupper($country);
    }

    public function getPreference()
    {
        return $this->preference;
    }

    public function setPreference($preference)
    {
        $this->preference = (float)$preference;
    }
}
