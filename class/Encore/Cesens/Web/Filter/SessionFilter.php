<?php

namespace Encore\Cesens\Web\Filter;

use Encore\Application\Filter\AbstractFilter;
use Encore\Application\Filter\FilterStack;
use Encore\Application\View\View;

class SessionFilter extends AbstractFilter
{
    public function filter(FilterStack $stack)
    {
        session_start();
        $_SESSION['cesens.com'] = [];
        $response = $stack->next();
        if ($response instanceof View) {
            $response->setVariable('session', $_SESSION['cesens.com']);
        }
        return $response;
    }
}
