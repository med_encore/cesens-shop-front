<?php

namespace Encore\Cesens\Web\Service;

use Encore\Cesens\Web\Model\Language;

class LanguageService
{
    public function getClientPreferredLanguages($acceptLanguage = null)
    {
        if ($acceptLanguage === null) {
            if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
                $acceptLanguage = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
            } else {
                return [];
            }
        }
        $languages = [];
        foreach (explode(',', $acceptLanguage) as $language) {
            $languages[] = Language::fromString($language);
        }
        usort($languages, [Language::class, 'sort']);
        return $languages;
    }

    public function getClientLanguage(array $available, $default)
    {
        if (!($default instanceof Language)) {
            $default = Language::fromString($default);
        }
        foreach ($available as $key => $lang) {
            if (!($lang instanceof Language)) {
                $available[$key] = Language::fromString($lang);
            }
        }
        $preferred = $this->getClientPreferredLanguages();
        $language  = $default;
        foreach ($preferred as $pref) {
            if (!($pref instanceof Language)) {
                $pref = Language::fromString($pref);
            }
            foreach ($available as $av) {
                if (!($av instanceof Language)) {
                    $av = Language::fromString($av);
                }
                if ($pref->getLanguage() === $av->getLanguage()) {
                    if ($pref->getCode() === $av->getCode()) {
                        return $av;
                    } else {
                        $language = $av;
                    }
                }
            }
        }
        return $language;
    }
}
