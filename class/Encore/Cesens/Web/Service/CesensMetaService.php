<?php

namespace Encore\Cesens\Web\Service;

use Encore\Application\Model\ModelDb;
use Encore\Cesens\Web\Model\Cliente;
use Encore\Cesens\Web\Model\Language;

class CesensMetaService extends ModelDb
{
    private $language;

    public function __construct(Language $language = null, \PDO $pdo = null)
    {
        $this->language = $language;
        $this->setPdo($pdo);
    }

    public function numeroDatos()
    {
        $result = $this->select('SELECT fecha, datos + datosDerivados AS numero FROM Estadisticas ORDER BY fecha DESC LIMIT 1');
        if (empty($result)) {
            return 252324620;
        } else {
            $fecha = \DateTime::createFromFormat('Y-m-d H:i:s', $result[0]['fecha']);
            $diferencia = (int)((time() - $fecha->getTimestamp()) / 60) * 1025 + (int)$fecha->format('i');
            return $result[0]['numero'] + $diferencia;
        }
    }

    public function tiposSensores()
    {
        return array_map(function ($sensor) {
            return $sensor['fabricante'] . ' ' . $sensor['referencia'];
        }, $this->getAll('Sensor'));
    }

    public function metricasDirectas(Language $language = null)
    {
        switch ($this->getLanguageCode($language)) {
            case 'es':
                return [
                    'Temperatura ambiente',
                    'Humedad relativa',
                    'Tiempo de humectación',
                    'Porcentaje de humectación',
                    'Temperatura en suelo',
                    'Estrés hídrico',
                    'Contenido volumétrico de agua',
                    'Presión atmosférica',
                    'Velocidad del viento',
                    'Dirección del viento',
                    'Racha máxima de viento',
                    'Radiación solar',
                    'Radiación solar UV',
                    'Precipitaciones',
                    'Dendrometría',
                ];
            default:
                return [
                    'Temperature',
                    'Relative humidity',
                    'Leaf wetness time',
                    'Leaf wetness percentage',
                    'Ground temperature',
                    'Hydric stress',
                    'Volumetric water content',
                    'Atmospheric pressure',
                    'Wind speed',
                    'Wind direction',
                    'Maximum wind gust',
                    'Solar radiation',
                    'UV solar radiation',
                    'Rainfall',
                    'Dendrometry',
                ];
        }
    }

    public function metricasDerivadas(Language $language = null)
    {
        switch ($this->getLanguageCode($language)) {
            case 'es':
                return [
                    'Temperatura media',
                    'Temperatura mínima',
                    'Temperatura máxima',
                    'Integral térmica activa',
                    'Integral térmica efectiva',
                    'Actividad vegetativa',
                    'Horas frío',
                    'Diferencia de temp. día-noche',
                    'Punto de rocío',
                    'Insolación diaria',
                    'Evapotraspiración',
                    'Radiación solar diaria',
                    'Precipitaciones diarias',
                    'Precipitación efectiva',
                    'Agua útil',
                    'Crecimiento diario (dendrometría)',
                    'Contracción diaria (dendrometría)',
                    'Índice heliotérmico'
                ];
            default:
                return [
                    'Mean temperature',
                    'Minimum temperature',
                    'Maximum temperature',
                    'Active thermal integral',
                    'Effective thermal integral',
                    'Vegetative activity',
                    'Chilling hours',
                    'Temperature diff / day-night',
                    'Dew point',
                    'Daily insolation',
                    'Evapotranspiration',
                    'Accumulated solar radiation',
                    'Daily rainfall',
                    'Effective rainfall',
                    'Useful water',
                    'Daily growth (dendrometry)',
                    'Daily contraction (dendrometry)',
                    'Heliothermic index'
                ];
        }
    }

    public function modelosPredictivos(Language $language = null)
    {
        switch ($this->getLanguageCode($language)) {
            case 'es':
                return [
                    'Mildiu de la vid',
                    'Oídio de la vid',
                    'Botrytis de la vid',
                    'Moteado del manzano',
                    'Moteado del peral',
                ];
            default:
                return [
                    'Grape mildew',
                    'Grape oidium',
                    'Grape botrytis',
                    'Pear scab',
                    'Apple scab',
                ];
        }
    }

    public function listarClientes()
    {
        return [
            new Cliente('pernodricard', 'Pernod Ricard', 'https://www.pernod-ricard.com/'),
            new Cliente('barondeley', 'Barón de Ley', 'http://www.barondeley.com/'),
            new Cliente('ramonbilbao', 'Ramón Bilbao', 'http://www.bodegasramonbilbao.es/'),
            new Cliente('riojanas', 'Bodegas Riojanas', 'http://bodegasriojanas.com/'),
            new Cliente('francoespanolas', 'Franco-Españolas', 'http://francoespanolas.com/'),
            new Cliente('altanza', 'Altanza', 'http://bodegasaltanza.com/'),

            new Cliente('ontanon', 'Ontañón', 'http://ontanon.es/es/'),
            new Cliente('ysios', 'Ysios', 'http://www.clubysios.com/'),
            new Cliente('campoviejo', 'Campo Viejo', 'https://www.campoviejo.com/'),
            new Cliente('tarsus', 'Tarsus', 'http://www.tarsusvino.com/unomasuno/'),
            new Cliente('aura', 'Aura (Rueda)'),
            new Cliente('bosquematasnos', 'Bosque de Matasnos', 'http://www.bosquedematasnos.es/'),

            new Cliente('gilberzal', 'Gil Berzal', 'http://www.gilberzal.es/'),
            new Cliente('carraovejas', 'Pago de Carraovejas', 'https://www.pagodecarraovejas.com/'),
            new Cliente('egurenugarte', 'Eguren Ugarte', 'http://egurenugarte.com/'),
            new Cliente('valenciso', 'Valenciso', 'https://www.valenciso.com/'),
            new Cliente('ossian', 'Ossian', 'http://www.ossianvinos.com/'),
            new Cliente('montecillo', 'Montecillo'),

            new Cliente('carlossancha', 'Juan Carlos Sancha', 'https://juancarlossancha.com/'),
            new Cliente('beldui', 'Beldui Txakolina'),
            new Cliente('ostatu', 'Ostatu', 'http://www.ostatu.com/es/'),
            new Cliente('neiker', 'NEIKER-Tecnalia', 'http://www.neiker.net/'),
            new Cliente('ur', 'Universidad de La Rioja', 'https://www.unirioja.es/'),
            new Cliente('icvv', 'Instituto de Ciencias de la Vid y del Vino', 'http://www.icvv.es/'),
        ];
    }

    public function listarRedesPublicas()
    {
        return [
            'AEMET',
            'SIAR La Rioja',
            'Oficina del Regante',
            'SAIH Ebro',
            'Info Riego',
            'Euskalmet',
            'Meteocat',
        ];
    }

    private function getLanguageCode(Language $language = null)
    {
        if ($language !== null) {
            return $language->getCode();
        } elseif ($this->language !== null) {
            return $this->language->getCode();
        } else {
            return null;
        }
    }
}
