<?php

namespace Encore\Cesens\Web\Service;

use Encore\Config\Config;

class TranslateService extends Config
{
    public function __construct($languageFile)
    {
        parent::__construct(include $languageFile);
    }

    public function get($key, $params = [])
    {
        if (!is_array($params)) {
            $params = [$params];
        }
        return vsprintf(parent::get($key, $key), $params);
    }

    public function __invoke($key, $params = [])
    {
        return $this->get($key, $params);
    }
}
