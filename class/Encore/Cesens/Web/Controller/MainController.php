<?php

namespace Encore\Cesens\Web\Controller;

use Encore\Application\Controller\AbstractActionController;
use Encore\Cesens\Web\Model\Language;
use Encore\Cesens\Web\Service\CesensMetaService;
use Encore\Cesens\Web\Service\TranslateService;
use PDO;
use PHPMailer\PHPMailer\PHPMailer;

class MainController extends AbstractActionController
{
    private $pdo;

    public function getPdo()
    {
        if ($this->pdo === null) {
            $params = $this->getModule()->getConfig('database');
            if ($params === null) {
                throw new \Exception('No database connection parameters');
            }
            $this->pdo = new PDO(
                $params['dsn'],
                $params['user'],
                $params['passwd'],
                [
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'',
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING,
                    PDO::ATTR_EMULATE_PREPARES => false,
                ]
            );
        }
        return $this->pdo;
    }

    public function defaultAction()
    {
        $i18n = new TranslateService($this->getApplication()->getDataPath('/i18n/' . $this->language()->getCode() . '.php'));
        $meta = new CesensMetaService($this->language(), $this->getPdo());
        return $this->view('/home.phtml', [
            '_'    => $i18n,
            'meta' => [
                'numeroDatos'        => $meta->numeroDatos(),
                'tiposSensores'      => $meta->tiposSensores(),
                'metricasDirectas'   => $meta->metricasDirectas(),
                'metricasDerivadas'  => $meta->metricasDerivadas(),
                'modelosPredictivos' => $meta->modelosPredictivos(),
                'clientes'           => $meta->listarClientes(),
                'redesPublicas'      => $meta->listarRedesPublicas(),
            ],
        ]);
    }

    public function condicionesGet()
    {
        return $this->view('/condiciones.phtml', [
            '_'    => new TranslateService($this->getApplication()->getDataPath('/i18n/' . $this->language()->getCode() . '.php')),
        ]);
    }

    public function contactPost()
    {
        $status  = 'SUCCESS';
        $name    = $this->getPost('name');
        $email   = $this->getPost('email');
        $message = $this->getPost('message');
        if (empty($name)) {
            $status = 'ERROR';
        }
        if (empty($email) || !preg_match('/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i', $email)) {
            $status = 'ERROR';
        }
        if (empty($message)) {
            $status = 'ERROR';
        }
        if ($status === 'SUCCESS') {
            $mailer = new PHPMailer();
            $mailer->isMail();
            $mailer->addAddress('info@encore-lab.com', 'Encore Lab');
            $mailer->addCC('sonia@encore-lab.com');
            $mailer->Subject = 'Contacto web Cesens';
            $mailer->From = $email;
            $mailer->FromName = $name;
            $mailer->isHTML(false);
            $mailer->Body = $message;
            if (!$mailer->send()) {
                $status = 'ERROR';
            }
        }
        return $this->view('/json.phtml', [
            'data' => [
                'status' => $status,
                'data'   => [
                    'name'    => $name,
                    'email'   => $email,
                    'message' => $message,
                ],
            ]
        ]);
    }

    /**
     * @return Language
     */
    public function language()
    {
        return $_SESSION['cesens.com']['language'];
    }
}
