<?php

return [
    'agricultura20' => 'Agriculture 4.0',
    'formato' => [
        'decimal' => '.',
        'miles'   => ',',
    ],
    'meta' => [
        'keywords' => 'agroclimatic stations, agro-climatic stations, la rioja, spain, cesens, vineyard, evineyard, agriculture, precision, agro-climatic information, agroclimatic information, water needs, cloud, encore lab',
        'description' => 'Cesens® is an agro-climatic information system to support decision making in agriculture. Know precisely the status of your crops.',
    ],
    'menu' => [
        'video'      => 'Video',
        'resumen'    => 'Summary',
        'funciones'  => 'Features',
        'beneficios' => 'Benefits',
        'datos'      => 'Data',
        'clientes'   => 'Clients',
        'contacto'   => 'Contact',
        'app'        => 'Application',
        'blog'       => 'Blog',
        'acceso'     => 'Login to the application',
    ],
    'acceso' => [
        'usuario'   => 'User or email',
        'clave'     => 'Password',
        'acceder'   => 'Log in',
        'recordar'  => 'Remember password',
        'registrar' => 'Don\'t have an account? Sign up',
        'errores'   => [
            'no-nombre'  => 'Enter a user or email',
            'no-clave'   => 'Enter your password',
            'incorrecto' => 'Invalid login data',
            'interno'    => 'An internal error occurred',
        ],
    ],
    'video' => [
        'ver' => 'Play video',
        'banner' => [
            'texto'    => 'Now you can use Cesens<sup>®</sup> at no cost',
            'mas-info' => 'More information',
        ],
    ],
    'resumen' => [
        'titulo'    => 'Welcome to Agriculture 4.0',
        'subtitulo' => 'Know precisely the status of your crops.',
        'intro' => [
            'titulo' => 'Cesens<sup>®</sup> is an agro-climatic information system to support decision making in agriculture.',
            'texto'  => 'It is based on stations that gather data from the crops for obtaining added-value information such as the risk of infection or the need of water.',
        ],
        'estaciones' =>  [
            'titulo' => 'Stations',
            'texto'  => 'Cesens<sup>®</sup> agro-climatic stations are placed in the cultivation parcels and they gather and send data to the cloud platform in real time.',
            'img'    => 'Cesens<sup>®</sup> station on the field',
        ],
        'cloud' =>  [
            'titulo' => 'Cloud',
            'texto'  => 'The cloud platform receives the data gathered in the fields and process them, obtaining high-value information for the agronomists.',
            'img'    => 'Cesens<sup>®</sup> cloud platform',
        ],
        'apps' =>  [
            'titulo' => 'Apps',
            'texto'  => 'All the information and functionality provided by Cesens<sup>®</sup> is accessible from any type of device and anywhere in the world.',
            'img'    => 'Cesens<sup>®</sup> en cualquier multidispositivo',
        ],
    ],
    'entrevistas' => [
        'titulo'    => 'Did you know…',
        'subtitulo' => '…having precise agro-climatic information is essential for making the best decisions within the management of any type of crop?',
        'texto'     => 'Cesens<sup>®</sup> gives to the agronomists the ability of knowing the actual status of each cultivation parcel, what allows them to optimize the management and to reduce costs.',
    ],
    'estacion-viñedo' => [
        'titulo'    => 'Monitor any type of crop',
    ],
    'funciones' => [
        'titulo'     => 'Agriculture of the 21st century',
        'subtitulo'  => 'A more precise and efficient agriculture.',
        'titulo2'    => 'Don’t miss anything',
        'subtitulo2' => 'Cesens<sup>®</sup> watches your crops for you.',
        'plagas' => [
            'titulo' => 'Pests',
            'texto'  => 'Cesens<sup>®</sup> lets you know the risk of infection of pests and diseases for you to apply their treatments only when it is necessary.',
            'img'    => 'Risk of infection',
        ],
        'riego' => [
            'titulo' => 'Watering',
            'texto'  => 'Cesens<sup>®</sup> monitors the hydric stress and/or volumetric water content of the ground so you can water your crops in the optimal quantity and moment.',
            'img'    => 'Need for watering',
        ],
        'seguimiento' => [
            'titulo' => 'Tracking',
            'texto'  => 'With Cesens<sup>®</sup> you can control the evolution of your crops taking into account any agro-climatic variable and phenological stage.',
            'img'    => 'Evolution of the crop',
        ],
        'historico' => [
            'titulo' => 'History',
            'texto'  => 'Cesens<sup>®</sup> stores all the information of your crops and lets you query historical data, even compare them with those of previous seasons.',
            'img'    => 'History of the crop',
        ],
        'alertas' => [
            'titulo' => 'Alerts',
            'intro'  => 'We know you are really busy, and so Cesens<sup>®</sup> warns you whenever something important occurs.',
            'texto'  => 'Cesens<sup>®</sup> has a powerful, fully-configurable alert system that allows you to be aware of what’s happening in your crops in real time, from anywhere.',
            'img'    => 'Customized alerts',
        ],
        'informes' => [
            'titulo' => 'Reports',
            'intro'  => 'Keep track of the evolution of your crops in the simplest way: from your e-mail.',
            'texto'  => 'Cesens<sup>®</sup> allows you to configure periodical reports with the information you consider most valuable for the tracking of your crops. Just choose the data you want to receive and the periodicity.',
            'img'    => 'Custom-taylored reports',
        ],
    ],
    'beneficios' => [
        'titulo'    => 'Big advantages',
        'subtitulo' => 'Optimize the use of pesticides and water with Cesens<sup>®</sup>.',
        'calidad' => [
            'titulo' => 'Better quality',
            'texto'  => 'Reducing the use of pesticides and watering at the appropriate moment allows you to obtain healthier and better harvests.',
            'img'    => 'Obtain better quality harvests',
        ],
        'costes' => [
            'titulo' => 'Lower costs',
            'texto'  => 'Watering and applying treatments at the right moment instead of doing it in advance supposes big savings in production costs.',
            'img'    => 'Reduce your production costs',
        ],
        'medioambiente' => [
            'titulo' => 'Environmental care',
            'texto'  => 'Sustainable management of water and pesticides lets you reduce environment pollution and preserve the hydric resources.',
            'img'    => 'Be more environmentally friendly',
        ],
    ],
    'datos' => [
        'titulo'     => 'In-depth view',
        'subtitulo' => 'You have never had so much information about your crops.',
        'recogidos' => 'Data gathered',
        'tipos' => [
            'titulo' => 'Types of sensor',
            'texto'  => 'Cesens<sup>®</sup> supports an ample variety of sensors from many manufacturers.',
            'lista' => [
                'titulo'  => 'The list is too long!',
                'texto'   => 'We have a catalog of sensors in which manufacturers, models and particular features are detailed.',
                'masInfo' => 'More information',
            ],
            'simultaneos' => [
                'titulo' => 'Simultaneous sensors',
                'texto'  => 'Cesens<sup>®</sup> allows to connect up to %s sensors at the same time.',
            ],
        ],
        'directas' => [
            'titulo' => 'Direct metrics',
            'texto'  => 'They are the data gathered directly from the sensors.',
        ],
        'derivadas' => [
            'titulo' => 'Derived metrics',
            'texto'  => 'They are obtained from processing the direct data.',
        ],
        'modelos' => [
            'titulo' => 'Predictive models',
            'texto'  => 'They show the risk of infection of the plant.',
        ],
        'mas' => [
            'titulo'    => 'Do you want more?',
            'subtitulo' => 'We can adapt Cesens<sup>®</sup> functionality to your needs.',
            'contacta'  => 'Contact us',
        ],
    ],
    'clientes' => [
        'titulo'    => 'Customers',
        'subtitulo' => 'Some of the customers who already trust Cesens<sup>®</sup>',
    ],
    'app' => [
        'titulo'    => 'Your crop comes first for us',
        'subtitulo' => 'Now Cesens<sup>®</sup> is for FREE',
        'texto'     => 'As part of our commitment to the evolution of agriculture, we offer you free of charge all Cesens<sup>®</sup>  functionality to work with data from public stations. Among other things you can:',
        'funciones' => [
            'alertas' => [
                'titulo' => 'Alerts',
                'texto'  => 'Receive alerts: forecast frost, rain, wind…',
            ],
            'informes' => [
                'titulo' => 'Reports',
                'texto'  => 'Configure automatic reports',
            ],
            'prediccion' => [
                'titulo' => 'Weather forecasting',
                'texto'  => 'Check the weather forecast',
            ],
            'graficas' => [
                'titulo' => 'Data',
                'texto'  => 'Analyze data using interactive graphs',
            ],
        ],
        'estacionesPublicas' => '7 redes públicas',
        'registro' => [
            'titulo' => 'Don\'t miss this opportunity!',
            'web' => [
                'titulo' => 'Web application',
                'texto'  => 'Access all available features and functionality of Cesens<sup>®</sup> directly from your browser',
                'boton'  => 'Sign up',
            ],
            'movil' => [
                'titulo' => 'Mobile app',
                'texto'  => 'Ideal for consulting climate information and weather forecasting in the easiest way',
            ],
        ],
    ],
    'contacto' => [
        'titulo'    => 'Nice to meet you!',
        'subtitulo' => 'We are at your disposal for any questions.',
        'nombre'    => 'Name',
        'email'     => 'Email',
        'mensaje'   => 'Message',
        'enviar'    => 'Send',
        'enviando'  => 'Sending',
        'exito' => [
            'titulo' => 'Message sent',
            'texto'  => 'Thank you for contacting us. We will reply you soon.',
        ],
        'error' => [
            'titulo' => 'Error',
            'texto'  => 'Your message could not be sent. Please, try again later.',
        ],
    ],
    'creditos' => [
        'productoDe' => 'is a product of',
        'derechos'   => 'All rights reserved',
    ],
];
