<?php

chdir(dirname(__DIR__));

require_once './vendor/encore/encore-framework/lib/Encore/Application/Application.php';

(new Encore\Application\Application())->main();
